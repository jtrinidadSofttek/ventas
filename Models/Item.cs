﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Item
    {
        public int Id { get; set; }

        public virtual Product Product { get; set; }

        public int ProductId { get; set; }

        public int TicketId { get; set; }

        public virtual Ticket Ticket { get; set; }

        [Range(1, double.MaxValue)]
        public double Quanty { get; set; }

        public double Total { get; set; }
    }
}
