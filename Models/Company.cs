﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models
{
    public class Company
    {
        public int Id { get; set; }

        [Required]
        public string Cuit { get; set; }

        [Required]
        public string Direction { get; set; }

        [Required]
        public string Name { get; set; }

        public ICollection<CreditNote> CreditNotes { get; set; }

        public ICollection<Ticket> Tickets { get; set; }
    }
}
