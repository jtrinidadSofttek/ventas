﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class CreditNote
    {
        public int Id { get; set; }

        [Required]
        public int CreditNumber { get; set; }

        [Required]
        public DateTime DateOfNote { get; set; }

        [Required]
        public virtual Company Company { get; set; }

        public int IdCompany { get; set; }

        [Required]
        public virtual Client Client { get; set; }

        public int IdClient { get; set; }

        [StringLength(100,MinimumLength = 5)]
        [Required]
        public string Details { get; set; }

        [Required]
        public double SubTotal { get; set; }

        [Required]
        public double Total { get; set; }

        [Required]
        public int IdPaymentMethod { get; set; }


    }
}

