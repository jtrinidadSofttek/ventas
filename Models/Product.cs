﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models
{
    public class Product
    {
     
        public int Id { get; set; }

        [Range(1, double.MaxValue)]
        public double Price { get; set; }

        [StringLength(100, MinimumLength = 5)]
        public string Details { get; set; }

        public virtual Item Item { get; set; }
    }
}
