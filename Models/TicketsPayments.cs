﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class TicketsPayments
    {
        public int TicketId { get; set; }

        public virtual Ticket Ticket { get; set; }

        public int PaymentId { get; set; }

        public virtual Payments Payments { get; set; }
    }
}
