﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class PaymentMethod
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
