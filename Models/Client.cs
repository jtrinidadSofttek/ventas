﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models
{
    public class Client
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public int ClientNumber { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Mail { get; set; }

        [Required]
        public string Adress { get; set; }

        [Required]
        [RegularExpression("^(20|23|27|30|33)-[0-9]{8}-[0-9]$")]
        public string Document { get; set; } // CUIT-CUIL

        public ICollection<CreditNote> CreditNotes { get; set; }

        public ICollection<Payments> Payments { get; set; }

        public ICollection<Ticket> Tickets { get; set; }
    }
}
