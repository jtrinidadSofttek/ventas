﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models
{
    public class Payments
    {
        public int Id { get; set; }

        [Required]
        public int PaymentNumber { get; set; }

        [Required]
        public virtual Client Client {get; set;}

        public int IdClient { get; set; }


        public IList<TicketsPayments> TicketsPayments { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public double Balance { get; set; }
    }
}
