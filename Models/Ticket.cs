﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Ticket
    {
        public int Id { get; set; }

        [Required]
        public int TicketNumber { get; set; }

        public string IdentityUnique { get; set; }

        //[Required]
        public DateTime DateOfSale { get; set; }

        //[Required]
        public virtual Client Client { get; set; }

        public int IdClient { get; set; }

        //[Required]
        public double SubTotal { get; set; }

       // [Required]
        public double Total { get; set; }

        //[Required]
        public double Payment { get; set; }

       // [Required]
        public virtual Company Company { get; set; }
        public int IdCompany { get; set; }

        //[Required]
        public string Details { get; set; }

        //[Required]
        public int IdPaymentMethod { get; set; }

        [Required]
        public bool Paid { get; set; }

        public virtual ICollection<Item> Items { get; set; }

        public IList<TicketsPayments> TicketsPayments { get; set; }

       // [Required]
        [Range(1,1)]
        public string TypeTicket { get; set; }

        //[Required]
        public string PrefixTicket { get; set; } // Prefijo de la factura

    }
}
