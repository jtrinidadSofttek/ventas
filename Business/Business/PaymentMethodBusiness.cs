﻿using DataAccess.Repositories;
using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Business
{
    public class PaymentMethodBusiness : IBusiness<PaymentMethod>
    {
        private PaymentMethodRepository repository { get; }

        public PaymentMethodBusiness(PaymentMethodRepository _rp) => this.repository = _rp;

        public IEnumerable<PaymentMethod> GetAll() => repository.GetAll();

        public PaymentMethod GetById(int id) => repository.GetById(id);

        public void Insert(PaymentMethod paymentMethod) => repository.Insert(paymentMethod);

        public void Delete(PaymentMethod paymentMethod) => repository.Delete(paymentMethod);

        public void Update(PaymentMethod paymentMethod) => repository.Update(paymentMethod);

    }
}
