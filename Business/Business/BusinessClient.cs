﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;
using Business.Business;
using DataAccess.Repositories;

namespace Business
{
    public class BusinessClient : IBusiness<Client>
    {
        private ClientRepository rp { get; }

        public BusinessClient(ClientRepository _rp) => this.rp = _rp;

        public IEnumerable<Client> GetAll() => rp.GetAll();

        public Client GetById(int id) => rp.GetById(id);

        public void Insert(Client client) => rp.Insert(client);

        public void Delete(Client client) => rp.Delete(client);

        public void Update(Client client) => rp.Update(client);

    }
}
