﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;
using DataAccess.Repositories;

namespace Business.Business
{
    public class BusinessCreditNote : IBusiness<CreditNote>
    {
        private CreditNoteRepository repository { get; }

        public BusinessCreditNote(CreditNoteRepository _rp) => this.repository = _rp;

        public IEnumerable<CreditNote> GetAll() => repository.GetAll();

        public CreditNote GetById(int id) => repository.GetById(id);

        public IEnumerable<CreditNote> GetByCreditNoteNumber(int number) => repository.GetByCreditNoteNumber(number);

        public void Insert(CreditNote creditNote) => repository.Insert(creditNote);

        public void Delete(CreditNote creditNote) => repository.Delete(creditNote);

        public void Update(CreditNote creditNote) => repository.Update(creditNote);
    }
}
