﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;
using DataAccess.Repositories;

namespace Business.Business
{
    public class BusinessItem : IBusiness<Item>
    {
        private ItemRepository rp { get; }

        public BusinessItem(ItemRepository _rp) => this.rp = _rp;

        public IEnumerable<Item> GetAll() => rp.GetAll();

        public Item GetById(int id) => rp.GetById(id);

        public void Insert(Item item) => rp.Insert(item);

        public void Delete(Item item) => rp.Delete(item);

        public void Update(Item item) => rp.Update(item);

        public double CalculateTotal(Item item) => item.Product.Price * item.Quanty;

    }
}
