﻿using DataAccess.Repositories;
using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Business
{
    public class ProductBusiness : IBusiness<Product>
    {
        private ProductRepository repository { get; }

        public ProductBusiness(ProductRepository _rp) => this.repository = _rp;

        public IEnumerable<Product> GetAll() => repository.GetAll();

        public Product GetById(int id) => repository.GetById(id);

        public void Insert(Product product) => repository.Insert(product);

        public void Delete(Product product) => repository.Delete(product);

        public void Update(Product product) => repository.Update(product);
    }
}
