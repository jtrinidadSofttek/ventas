﻿using DataAccess.Repositories;
using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Business
{
    public class TicketBusiness : IBusiness<Ticket>
    {
        private TicketRepository repository { get; }

        private ItemRepository rp { get; }

        public TicketBusiness(TicketRepository _rp) => this.repository = _rp;

        public IEnumerable<Ticket> GetAll() => repository.GetAll();

        public Ticket GetById(int id) => repository.GetById(id);

        public List<Ticket> GetTicketsforClient(int idClient) => repository.GetTicketsforClient(idClient);

        public List<Ticket> GetTicketsUnpad() => repository.GetTicketsUnpad();

        public Ticket GetByIdentityUnique(string identity) => repository.GetByIdentityUnique(identity);

        public void Insert(Ticket ticket) => repository.Insert(ticket);

        public void Delete(Ticket ticket) => repository.Delete(ticket);

        public void Update(Ticket ticket) => repository.Update(ticket);

        public void CalculateTotalItems( Ticket ticket)
        {
            foreach (var item in ticket.Items )
            {
                ticket.SubTotal = new BusinessItem(rp).CalculateTotal(item);
            }
        }
    }
}
