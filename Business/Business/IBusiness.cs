﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Business
{
    public interface IBusiness<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();

        TEntity GetById(int id);

        void Insert(TEntity obj);

        void Delete(TEntity obj);

        void Update(TEntity obj);


    }

}
