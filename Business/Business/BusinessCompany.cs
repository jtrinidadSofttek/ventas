﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess.Repositories;
using Models;

namespace Business.Business
{
    public class BusinessCompany : IBusiness<Company>
    {
        private CompanyRepository rp { get; }

        public BusinessCompany(CompanyRepository _rp) => this.rp = _rp;

        public IEnumerable<Company> GetAll() => rp.GetAll();

        public Company GetById(int id) => rp.GetById(id);

        public void Insert(Company company) => rp.Insert(company);

        public void Delete(Company company) => rp.Delete(company);

        public void Update(Company company) => rp.Update(company);
    }
}
