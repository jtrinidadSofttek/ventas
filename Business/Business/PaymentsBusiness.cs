﻿using DataAccess.Repositories;
using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Business
{
    public class PaymentsBusiness : IBusiness<Payments>
    {
        private PaymentsRepository repository { get; }

        public PaymentsBusiness(PaymentsRepository _rp) => this.repository = _rp;

        public IEnumerable<Payments> GetAll() => repository.GetAll();

        public Payments GetById(int id) => repository.GetById(id);

        public List<Payments> GetByPaymentNumber(int number) => repository.GetByPaymentNumber(number);

        public void Insert(Payments payments) => repository.Insert(payments);

        public void Delete(Payments payments) => repository.Delete(payments);

        public void Update(Payments payments) => repository.Update(payments);

        public int Pay(double pay, Ticket ticket) //retorna el numero de pago. 
        {
            if(ticket.Payment <= pay)
            {
                ticket.Paid = true;
                Random rnd = new Random();
                Payments payments = new Payments();
                payments.Client = ticket.Client;
                payments.Date = DateTime.Now;
                payments.IdClient = ticket.IdClient;
                payments.PaymentNumber = rnd.Next();
                payments.Balance = pay - ticket.Payment;
                Insert(payments);
                return payments.PaymentNumber;
            }
            else
            {
                ticket.Paid = false;
                Random rnd = new Random();
                Payments payments = new Payments();
                payments.Client = ticket.Client;
                payments.Date = DateTime.Now;
                payments.IdClient = ticket.IdClient;
                payments.PaymentNumber = rnd.Next();
                payments.Balance = pay - ticket.Payment;
                Insert(payments);
                return payments.PaymentNumber;
            }
        }
    }
}
