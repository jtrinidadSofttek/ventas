﻿using Microsoft.EntityFrameworkCore;
using Models;

namespace DataAccess
{
    public class SalesContext : DbContext
    {
        public SalesContext() { }

        public SalesContext(DbContextOptions<SalesContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CreditNote>()
                .HasOne<Company>(cd => cd.Company)
                .WithMany(c => c.CreditNotes)
                .HasForeignKey(cd => cd.IdCompany)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<CreditNote>()
               .HasOne<Client>(cd => cd.Client)
               .WithMany(c => c.CreditNotes)
               .HasForeignKey(cd => cd.IdClient)
               .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Payments>()
                .HasOne<Client>(p => p.Client)
                .WithMany(c => c.Payments)
                .HasForeignKey(p => p.IdClient)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Ticket>()
              .HasOne<Client>(t => t.Client)
              .WithMany(c => c.Tickets)
              .HasForeignKey(t => t.IdClient)
              .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Ticket>()
                .HasOne<Company>(t => t.Company)
                .WithMany(c => c.Tickets)
                .HasForeignKey(t => t.IdClient)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Item>().
                HasOne<Ticket>(i => i.Ticket)
                .WithMany(t => t.Items).
                HasForeignKey(i => i.TicketId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TicketsPayments>()
                .HasKey(tp => new { tp.PaymentId, tp.TicketId });

            modelBuilder.Entity<TicketsPayments>().
                HasOne<Ticket>(tp => tp.Ticket)
                .WithMany(t => t.TicketsPayments).
                HasForeignKey(tp => tp.TicketId);

            modelBuilder.Entity<TicketsPayments>().
                HasOne<Payments>(tp => tp.Payments)
                .WithMany(p => p.TicketsPayments).
                HasForeignKey(tp => tp.PaymentId);

            modelBuilder.Entity<Item>().
                HasOne<Product>(i => i.Product)
                .WithOne(p => p.Item).
                HasForeignKey<Item>(i => i.ProductId);
        }

       /* protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=SalesDB;");
        }*/

        //Entidades

        public DbSet<Client> Clients { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<CreditNote> CreditNotes { get; set; }
        public DbSet<PaymentMethod> PaymentMethods { get; set; }
        public DbSet<Payments> Payments { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<TicketsPayments> TicketsPayments { get; set; }
    }
}
