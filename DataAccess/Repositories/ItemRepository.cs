﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Models;

namespace DataAccess.Repositories
{
    public class ItemRepository : IRepository<Item>
    {
        private SalesContext db { get; }

        public ItemRepository(SalesContext _db) => this.db = _db;

        public IEnumerable<Item> GetAll() => db.Items.Include(p => p.Product)
            .Include(t => t.Ticket).ToList();

        public Item GetById(int id) => db.Items.Include(p => p.Product)
            .Include(t => t.Ticket).FirstOrDefault(x => x.Id == id);

        public void Insert(Item item)
        {
            db.Add(item);
            db.SaveChanges();
        }

        public void Delete(Item item)
        {
            db.Remove(item);
            db.SaveChanges();
        }

        public void Update(Item item)
        {
            var itm = GetById(item.Id);
            itm.Product = item.Product;
            itm.ProductId = item.ProductId;
            itm.Quanty = item.Quanty;
            itm.Ticket = item.Ticket;
            itm.TicketId = item.TicketId;
            itm.Total = item.Total;
            db.SaveChanges();
        }
    }
}
