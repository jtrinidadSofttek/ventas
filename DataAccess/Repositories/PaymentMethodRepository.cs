﻿using Models;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess.Repositories
{
    public class PaymentMethodRepository : IRepository<PaymentMethod>
    {
        private SalesContext db { get; }

        public PaymentMethodRepository(SalesContext _db) => this.db = _db;

        public IEnumerable<PaymentMethod> GetAll() => db.PaymentMethods.ToList();

        public PaymentMethod GetById(int id) => db.PaymentMethods.FirstOrDefault(x => x.Id == id);

        public void Insert(PaymentMethod paymentMethod) {
            db.Add(paymentMethod);
            db.SaveChanges(); 
        }

        public void Delete(PaymentMethod paymentMethod)
        {
            db.Remove(paymentMethod);
            db.SaveChanges();
        }

        public void Update(PaymentMethod paymentMethod) {
            var pm = GetById(paymentMethod.Id);
            pm.Name = paymentMethod.Name;
            db.SaveChanges();
        }
    }
}
