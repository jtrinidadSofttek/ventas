﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Models;

namespace DataAccess.Repositories
{
    public class ClientRepository : IRepository<Client>
    {
        private SalesContext db { get; }

        public ClientRepository(SalesContext _db) => this.db = _db;

        public IEnumerable<Client> GetAll() => db.Clients.Include(p => p.Payments)
            .Include(t => t.Tickets)
            .Include(c => c.CreditNotes).ToList();

        public Client GetById(int id) => db.Clients.FirstOrDefault(x => x.Id == id);

        public Client GetByNumerClient(int number) => db.Clients.FirstOrDefault(x => x.ClientNumber == number);

        public void Insert(Client client)
        {
            if(client.Payments == null)
            {
                client.Payments = new List<Payments>();
            }
            if(client.Tickets == null)
            {
                client.Tickets = new List<Ticket>();
            }
            if(client.CreditNotes == null)
            {
                client.CreditNotes = new List<CreditNote>();
            }

            db.Clients.Add(client);
            db.SaveChanges();
        }

        public void Delete(Client client)
        {
            db.Clients.Remove(client);
            db.SaveChanges();
        }

        public void Update(Client client)
        {
            var cli = GetById(client.Id);
            cli.Name = client.Name;
            cli.LastName = client.LastName;
            cli.Mail = client.LastName;
            cli.Payments = client.Payments; //
            cli.Tickets = client.Tickets; //
            cli.Adress = client.Adress;
            cli.CreditNotes = client.CreditNotes; //
            cli.Document = client.Document;
            db.SaveChanges();
        }
    }
}
