﻿using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Repositories
{
    public class PaymentsRepository: IRepository<Payments>
    {
        private SalesContext db { get; }

        public PaymentsRepository(SalesContext _db) => this.db = _db;

        public IEnumerable<Payments> GetAll() => db.Payments.Include(x => x.Client)
            .Include(t => t.TicketsPayments).ToList();

        public Payments GetById(int id) => db.Payments.Include(x=> x.Client)
            .Include(t => t.TicketsPayments).FirstOrDefault(x => x.Id == id);

        public List<Payments> GetByPaymentNumber(int number)
        {
            return db.Payments.ToList().Where<Payments>(p => p.PaymentNumber == number).ToList();
        }
        public void Insert(Payments payments)
        {
            if(payments.TicketsPayments == null)
            {
                payments.TicketsPayments = new List<TicketsPayments>();
            }
            db.Add(payments);
            db.SaveChanges();
        }

        public void Delete(Payments payments)
        {
            db.Remove(payments);
            db.SaveChanges();
        }

        public void Update(Payments payments)
        {
            var originalPayments = GetById(payments.Id);
            originalPayments.Client = payments.Client;
            originalPayments.Balance = payments.Balance;
            originalPayments.Date = payments.Date;
            originalPayments.IdClient = payments.IdClient;
            originalPayments.TicketsPayments = payments.TicketsPayments;
            db.SaveChanges();
        }
    }
}
