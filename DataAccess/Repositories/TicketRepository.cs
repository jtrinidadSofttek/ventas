﻿using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Repositories
{
    public class TicketRepository:IRepository<Ticket>
    {
        private SalesContext db { get; }

        public TicketRepository(SalesContext _db) => this.db = _db;

        public IEnumerable<Ticket> GetAll() => db.Tickets.Include(t => t.Client).Include(t => t.Company)
            .Include(t => t.TicketsPayments)
            .Include(t => t.Items).ToList();

        public Ticket GetById(int id) => db.Tickets.Include(t => t.Client).Include(t => t.Company)
            .Include(t => t.TicketsPayments)
            .Include(t => t.Items).FirstOrDefault(x => x.Id == id);

        public List<Ticket> GetTicketsforClient(int idClient)
        {
            List<Ticket> tickets = new List<Ticket>();
            tickets = GetAll().Where<Ticket>(t => t.IdClient == idClient).ToList();
            return tickets;
        }

        public List<Ticket> GetTicketsUnpad()
        {
            List<Ticket> tickets = new List<Ticket>();
            tickets = GetAll().Where<Ticket>(t => t.Paid == false).ToList();
            return tickets;
        }

        public Ticket GetByIdentityUnique(string identity) => db.Tickets.FirstOrDefault(t => t.IdentityUnique == identity);

        public void Insert(Ticket ticket)
        {
            if(ticket.TicketsPayments == null)
            {
                ticket.TicketsPayments = new List<TicketsPayments>();
            }
            if(ticket.Items == null)
            {
                ticket.Items = new List<Item>();
            }
            db.Add(ticket);
            db.SaveChanges();
        }

        public void Delete(Ticket ticket)
        {
            db.Tickets.Remove(ticket);
            db.SaveChanges();
        }

        public void Update(Ticket ticket)
        {
            var originalticket = GetById(ticket.Id);
            originalticket.IdClient = ticket.IdClient;
            originalticket.Client = ticket.Client;
            originalticket.Company = ticket.Company;
            originalticket.IdCompany = ticket.IdCompany;
            originalticket.IdPaymentMethod = ticket.IdPaymentMethod;
            originalticket.Paid = ticket.Paid;
            originalticket.Payment = ticket.Payment;
            originalticket.TicketNumber = ticket.TicketNumber;
            originalticket.TicketsPayments = ticket.TicketsPayments; //
            originalticket.Items = originalticket.Items; //
            originalticket.SubTotal = ticket.SubTotal;
            originalticket.Total = ticket.Total;
            db.SaveChanges();
        }
    }
}
