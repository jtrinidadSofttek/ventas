﻿using Models;
using System.Collections.Generic;
using System.Linq;


namespace DataAccess.Repositories
{
    public class ProductRepository :IRepository<Product>
    {
        private SalesContext db { get; }

        public ProductRepository(SalesContext _db) => this.db = _db;

        public IEnumerable<Product> GetAll() => db.Products.ToList();

        public Product GetById(int id) => db.Products.FirstOrDefault(x => x.Id == id);

        public void Insert(Product product)
        {
            db.Add(product);
            db.SaveChanges();
        }

        public void Delete(Product product)
        {
            db.Remove(product);
            db.SaveChanges();
        }

        public void Update(Product product)
        {
            var originalProduct = GetById(product.Id);
            originalProduct.Price = product.Price;
            originalProduct.Details = product.Details;
            originalProduct.Item = product.Item;
            db.SaveChanges();
        }
    }
}
