﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Models;

namespace DataAccess.Repositories
{
    public class CompanyRepository : IRepository<Company>
    {
        private SalesContext db { get; }

        public CompanyRepository(SalesContext _db) => this.db = _db;

        public IEnumerable<Company> GetAll() => db.Companies.Include(t => t.Tickets)
            .Include(c => c.CreditNotes).ToList();

        public Company GetById(int id) => db.Companies.Include(t => t.Tickets)
            .Include(c => c.CreditNotes).FirstOrDefault(x => x.Id == id);

        public void Insert(Company company)
        {
            if(company.Tickets == null)
            {
                company.Tickets = new List<Ticket>();
            }

            if(company.CreditNotes == null)
            {
                company.CreditNotes = new List<CreditNote>();
            }

            db.Companies.Add(company);
            db.SaveChanges();
        }

        public void Delete(Company company)
        {
            db.Companies.Remove(company);
            db.SaveChanges();
        }

        public void Update(Company company)
        {
            var com = GetById(company.Id);
            com.Name = company.Name;
            com.Cuit = company.Cuit;
            com.CreditNotes = company.CreditNotes;
            com.Direction = company.Direction;
            com.Tickets = company.Tickets;
            db.SaveChanges();
        }
    }
}
