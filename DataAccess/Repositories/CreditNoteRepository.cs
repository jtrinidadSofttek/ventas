﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Models;

namespace DataAccess.Repositories
{
    public class CreditNoteRepository : IRepository<CreditNote>
    {
        private SalesContext db { get; }

        public CreditNoteRepository(SalesContext _db) => this.db = _db;

        public IEnumerable<CreditNote> GetAll() => db.CreditNotes.Include(c => c.Client).Include(c => c.Company).ToList();

        public CreditNote GetById(int id) => db.CreditNotes.Include(c => c.Client).Include(c => c.Company).FirstOrDefault(x => x.Id == id);

        public IEnumerable<CreditNote> GetByCreditNoteNumber(int number)
        {
            return db.CreditNotes.ToList().Where<CreditNote>(c => c.CreditNumber == number).ToList();
        }

        public void Insert(CreditNote creditNote)
        {
            db.CreditNotes.Add(creditNote);
            db.SaveChanges();
        }

        public void Delete(CreditNote creditNote)
        {
            db.CreditNotes.Remove(creditNote);
            db.SaveChanges();
        }

        public void Update(CreditNote creditNote)
        {
            var cred = GetById(creditNote.Id);
            cred.Client = creditNote.Client;
            cred.CreditNumber = creditNote.CreditNumber;
            cred.Company = creditNote.Company;
            cred.DateOfNote = creditNote.DateOfNote;
            cred.Details = creditNote.Details;
            cred.IdClient = creditNote.IdClient;
            cred.IdCompany = creditNote.IdCompany;
            cred.IdPaymentMethod = creditNote.IdPaymentMethod;
            cred.SubTotal = creditNote.SubTotal;
            cred.Total = creditNote.Total;
            db.SaveChanges();
        }
    }
}
