﻿using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO.Pipes;
using System.Linq;
using System.Threading.Tasks;

namespace SalesUI.Models
{
    public class ClientViewModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Mail { get; set; }

        [Required]
        public string Adress { get; set; }

        [Required]
        [RegularExpression("^(20|23|27|30|33)-[0-9]{8}-[0-9]$")]
        public string Document { get; set; } // CUIT-CUIL

        public ClientViewModel(Client client)
        {
            Id = client.Id;
            Name = client.Name;
            LastName = client.LastName;
            Mail = client.Mail;
            Adress = client.Adress;
            Document = client.Document;
        }

        public void aClient()
        {
            Client client = new Client();
            client.Id = Id;
            client.Name = Name;
            client.LastName = LastName;
            client.Mail = Mail;
            client.Adress = Adress;
            client.Document = Document;
        }
    }
}
