﻿using Microsoft.AspNetCore.Mvc.ActionConstraints;
using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SalesUI.Models
{
    public class ItemViewModel
    {
        public int Id { get; set; }

        public string Product { get; set; }

        public int ProductId { get; set; }

        [Range(1, double.MaxValue)]
        public double Quanty { get; set; }

        public double Total { get; set; }

        public ItemViewModel() { }

        public ItemViewModel(Item item)
        {
            Id = item.Id;
            Product = item.Product.Details;
            ProductId = item.ProductId;
            Quanty = item.Quanty;
            Total = item.Total;
        }

        public Item aItem()
        {
            Item item = new Item();
            item.Id = Id;
            item.ProductId = ProductId;
            item.Quanty = Quanty;
            item.Total = Total;
            return item;
        }
    }

  
}
