﻿using Business;
using Business.Business;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Models;
using SalesUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SalesUI.Controllers
{
    public class TicketController : Controller
    {
        private readonly TicketBusiness ticketBusiness;
        private readonly BusinessClient clientBusiness;
        private readonly BusinessItem itemBusiness;
        private readonly ProductBusiness productBusiness;

        public TicketController(TicketBusiness _context, BusinessClient __context, BusinessItem context_, ProductBusiness context__)
        {
            ticketBusiness = _context;
            clientBusiness = __context;
            itemBusiness = context_;
            productBusiness = context__;
        }

        // GET: Ticket
        public ActionResult Index(string searchString)
        {
            var client = new Client();
            if (!String.IsNullOrEmpty(searchString))
            {
                client = clientBusiness.GetById(Convert.ToInt32(searchString));
            }
            if(client == null)
            {
                return ViewComponent("Error");
            }
            var clientview = new ClientViewModel(client);
            return View(clientview);
          
        }

        //Ticket/ListItem/ {id}
        public ActionResult ListItem(int id)
        {
            var client = clientBusiness.GetById(id);
            var clientview = new ClientViewModel(client);
            ViewBag.Id_Client = id;
            ViewBag.Id_products = new SelectList(productBusiness.GetAll(), "Id", "Details");
            return View();
        }

        public ActionResult AddItem(int clientid, ItemViewModel itemViewModel)
        {
            if (ModelState.IsValid)
            {
                Random rnd = new Random();
               Item item =  itemViewModel.aItem();
                item.Product = productBusiness.GetById(item.ProductId);
                item.Total = itemBusiness.CalculateTotal(item);
                item.TicketId = 3;
                ViewBag.Id_Client = clientid;
                itemBusiness.Insert(item);
            }
            return RedirectToAction("ListItem", new { id = clientid });
        }

        /* 
        // GET: Ticket/Create
        public ActionResult Create()
        {
            return View();
        }
        
        // POST: Ticket/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Ticket/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Ticket/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Ticket/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Ticket/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }*/
    }
}